﻿namespace GamCover2
{
    public enum State
    {
        T,
        F
    }

    internal class ConditionVertex : IVertex
    {
        private byte _nextFalseId;
        private byte _nextTrueId;

        public ConditionVertex(byte id, byte nextTrueId, byte nextFalseId, string condition)
        {
            Id = id;
            _nextTrueId = nextTrueId;
            _nextFalseId = nextFalseId;
            Condition = condition;
        }

        public State State { get; protected set; }

        public string Condition { get; protected set; }

        public byte Id { get; protected set; }

        public byte GetNextId()
        {
            return State == State.T ? _nextTrueId : _nextFalseId;
        }

        protected void SetNextFalseId(byte nextFalseId)
        {
            _nextFalseId = nextFalseId;
        }

        protected void SetNextTrueId(byte nextTrueId)
        {
            _nextTrueId = nextTrueId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var p = obj as ConditionVertex;
            if (p == null)
            {
                return false;
            }

            if (Id == p.Id && Condition == p.Condition)
            {
                State = State.T;
                p.State = State.T;
                if (GetNextId() == p.GetNextId())
                {
                    State = State.F;
                    p.State = State.F;
                    if (GetNextId() == p.GetNextId())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool Equals(ConditionVertex cv)
        {
            if (cv == null)
            {
                return false;
            }

            if (Id == cv.Id && Condition == cv.Condition)
            {
                State = State.T;
                cv.State = State.T;
                if (GetNextId() == cv.GetNextId())
                {
                    State = State.F;
                    cv.State = State.F;
                    if (GetNextId() == cv.GetNextId())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (Id + Condition.Length + _nextFalseId + _nextTrueId).GetHashCode();
        }

        public override string ToString()
        {
            State backupState = State;
            State = State.T;
            byte nextTrueId = GetNextId();
            State = State.F;
            byte nextFalseId = GetNextId();
            State = backupState;
            return string.Format("CV [id={0} nextTrue={1} nextFalse={2} state={3}]", Id, nextTrueId, nextFalseId, State);
        }
    }
}