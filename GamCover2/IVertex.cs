﻿namespace GamCover2
{
    internal interface IVertex
    {
        byte Id { get; }
        byte GetNextId();
    }
}