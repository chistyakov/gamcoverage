﻿//using System.Xml;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using CubeCoverLib;

namespace GamCover2
{
    public class GamCover
    {
        private GAM _gam;

        public void LoadGamData(string inputFileName)
        {
            XDocument xDoc = XDocument.Load(inputFileName);

            //if (xDoc.Root == null) return;
            //XElement nmspcEl = xDoc.Root.Element("namespace");
            //if (nmspcEl == null) return;
            //XElement GAMsEl = nmspcEl.Element("GAMs");
            //if (GAMsEl == null) return;
            //XElement GAMEl = GAMsEl.Element("GAM");
            //if (GAMEl == null) return;
            //XElement nodes = GAMEl.Element("nodes");
            XElement xStartUid =
                xDoc.Root.Element("namespace").Element("GAMs").Element("GAM").Element("attributes").Element("startUid");

            XElement nodes = xDoc.Root.Element("namespace").Element("GAMs").Element("GAM").Element("nodes");
            if (nodes == null) return;
            var vertices = new HashSet<IVertex>();
            foreach (XElement x in nodes.Elements())
            {
                IVertex vertex;
                if (x.Name == "cv")
                {
                    vertex = ParseCV(x);
                }
                else if (x.Name == "lv")
                {
                    vertex = ParseLV(x);
                }
                else
                {
                    vertex = ParseUD(x);
                }
                vertices.Add(vertex);
            }
            Console.WriteLine("vertices:\n" + string.Join("\n", vertices));
            _gam = new GAM(vertices, Byte.Parse(xStartUid.Value));
        }

        public ICoverage GetResult()
        {
            return _gam.GetCoverage();
        }

        private static ConditionVertex ParseCV(XElement x)
        {
            byte uid = Byte.Parse(x.Attribute("uid").Value);
            byte nextTrueId = Byte.Parse(x.Element("trueOutput").Value);
            byte nextFalseId = Byte.Parse(x.Element("falseOutput").Value);
            string condition = x.Element("condition").Value;
            return new ConditionVertex(GetUid(x), nextTrueId, nextFalseId, condition);
        }

        private static LineVertex ParseLV(XElement x)
        {
            var fList = new List<string>();
            XElement fnctns = x.Element("functions");
            if (fnctns != null)
            {
                fList.AddRange(fnctns.Elements().Select(fnctn => fnctn.Value));
            }
            return new LineVertex(GetUid(x), ParseOutput(x), fList);
        }

        private static UnionDecision ParseUD(XElement x)
        {
            return new UnionDecision(GetUid(x), ParseOutput(x));
        }

        private static byte ParseOutput(XElement x)
        {
            string outStr = x.Element("output").Value;
            if (outStr == "Out")
                return (byte) SpecialId.Out;
            return Byte.Parse(outStr);
        }

        private static byte GetUid(XElement x)
        {
            return Byte.Parse(x.Attribute("uid").Value);
        }
    }
}