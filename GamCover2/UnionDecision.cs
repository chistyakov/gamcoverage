﻿namespace GamCover2
{
    internal class UnionDecision : IVertex
    {
        private byte _nextId;

        public UnionDecision(byte id, byte nextId)
        {
            Id = id;
            _nextId = nextId;
        }

        public byte Id { get; protected set; }

        public byte GetNextId()
        {
            return _nextId;
        }

        protected void SetNext(byte nextId)
        {
            _nextId = nextId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var p = obj as UnionDecision;
            if (p == null)
            {
                return false;
            }
            return (Id == p.Id && GetNextId() == p.GetNextId());
        }

        public bool Equals(UnionDecision ud)
        {
            if (ud == null)
            {
                return false;
            }
            return (Id == ud.Id && GetNextId() == ud.GetNextId());
        }

        public override int GetHashCode()
        {
            return (Id + GetNextId()).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("UD [id={0} next={1}]", Id, GetNextId());
        }
    }
}