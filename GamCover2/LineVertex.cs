﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace GamCover2
{
    internal class LineVertex : IVertex
    {
        private readonly List<string> _funcList = new List<string>();
        private byte _nextId;

        public LineVertex(byte id, byte nextId, List<string> funcList)
        {
            Id = id;
            _nextId = nextId;
            _funcList = funcList;
        }

        public byte Id { get; protected set; }

        public byte GetNextId()
        {
            return _nextId;
        }

        protected void AddFunc(string addFunc)
        {
            _funcList.Add(addFunc);
        }

        protected bool RemoveFunc(string item)
        {
            return _funcList.Remove(item);
        }

        public ReadOnlyCollection<string> GetFuncs()
        {
            return new ReadOnlyCollection<string>(_funcList);
        }

        protected void SetNext(byte nextId)
        {
            _nextId = nextId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var p = obj as LineVertex;
            if (p == null)
            {
                return false;
            }

            if (Id == p.Id && GetNextId() == p.GetNextId())
            {
                return GetFuncs().SequenceEqual(p.GetFuncs());
            }
            return false;
        }

        public bool Equals(LineVertex lv)
        {
            if (lv == null)
            {
                return false;
            }

            if (Id == lv.Id && GetNextId() == lv.GetNextId())
            {
                return GetFuncs().SequenceEqual(lv.GetFuncs());
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (Id + _nextId + _funcList.Sum(s => s.Length)).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("LV [id={0} next={1} functions: {2}]", Id, GetNextId(), string.Join("->", GetFuncs()));
        }
    }
}