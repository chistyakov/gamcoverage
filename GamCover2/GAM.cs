﻿using System;
using System.Collections.Generic;
using System.Linq;
using CubeCoverLib;

namespace GamCover2
{
    public enum SpecialId : sbyte
    {
        Out = 0,
        In = -1
    };

    internal class GAM
    {
        private byte _startId;
        private HashSet<IVertex> _vertices;

        public GAM(HashSet<IVertex> vertices, byte startId)
        {
            Vertices = vertices;
            _startId = startId;
        }

        public HashSet<IVertex> Vertices
        {
            get { return _vertices; }
            protected set
            {
                if (value.Any(vertex => vertex.GetNextId() == (sbyte) SpecialId.Out) &&
                    value.All(vertex => value.Any(vertex1 => vertex1.Id == vertex.GetNextId())))
                {
                    _vertices = value;
                }
            }
        }

        public ICoverage GetCoverage()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}